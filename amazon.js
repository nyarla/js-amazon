/*
amazon.js - link replacer for Amazon.co.jp
==========================================

Author:     Naoki OKAMURA (Nyarla) <nyarla[ at ]thotep.net>
License:    MIT-License
Repository: https://github.com/nyarla/js-amazon

*/

"use strict";

if ( typeof(window.nyarla) == 'undefined' )
    window.nyarla = {}


window.nyarla.initAmazonJS = function ( AssociateTag, Size ) {
    var SizeMap = {
          's':'THUMBZZZ'
        , 'm':'MZZZZZZZ'
        , 'l':'LZZZZZZZ'
    };
    var ImagePrefix = 'http://ec2.images-amazon.com/images/P/'; 
    var ImageSize   = SizeMap[Size];
    var LinkRe      = /^http:\/\/www\.amazon\.co\.jp\/gp\/product\/([A-Z0-9]{10})/;

    var ItemBuilder    = function ( asin, title, url ) {
        var container = document.createElement('figure');
        var caption   = document.createElement('figcaption');

        var imageSrc  = ImagePrefix + asin + '.01.' + ImageSize + '.jpg';
        var image     = document.createElement('img');
            image.setAttribute('src', imageSrc);
            image.setAttribute('title', title);
            image.setAttribute('alt', "Thumbnail");

        var imageLink = document.createElement('a');
            imageLink.setAttribute('href', url);
            imageLink.appendChild( image );

        var titleLink = document.createElement('a');
            titleLink.setAttribute('href', url);
            titleLink.appendChild( document.createTextNode(title) );

        caption.appendChild( titleLink );

        var embed     = document.createElement('p');
            embed.className = 'thumbnail';
            embed.appendChild(imageLink);

        container.appendChild( embed );
        container.appendChild( caption );
        container.className = 'amazonjs-widget';

        return container;
    }

    var ReplaceHandler = function ( associateTag ) {
        var lst = document.querySelectorAll('p > a[href^="http://www.amazon.co.jp/gp/product/"]:only-child');
        for ( var i = 0, len = lst.length; i < len; i++ ) {
            var elm   = lst[i];
            var href  = elm.getAttribute('href');
            var asin  = LinkRe.exec(href)[1];
            var title = elm.textContent;

            elm.parentNode.parentNode.replaceChild(
                ItemBuilder( asin, title, href ),
                elm.parentNode
            );
        }
    }

    if ( window.addEventListener ) {
        window.addEventListener('load', ReplaceHandler, false);
    }
}

