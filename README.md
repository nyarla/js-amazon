1. How to use
-------------

load and configure:

    <script type="text/javascript"
            src="https://raw.github.com/nyarla/js-amazon/master/amazon.js"></script>
    <script type="text/javascript">
        window.nyarla.initAmazonJS( 'nyarlanet-22', 'm' );
    </script>

and write amazon.co.jp link:

    <p><a href="http://www.amazon.co.jp/gp/product/4041007585">レンタルマギカ 未来の魔法使い (角川スニーカー文庫)</a></p>


2. Example Output
-----------------

    <figure class="amazonjs-widget">
        <p class="thunmbnail">
            <a href="{{LinkToAmazon}}">
                <img src="{{LinkToImage}}"
                     title="{{TitleOfItem}}"
                     alt="Thumbnail">
            </a>
        </p>
        <figcaption>
            <a href="{{LinkToAmazon}}">{{TitleOfItem}}</a>
        </figcaption>
    </figure>

3. Copyright and License
------------------------

(C) 2013 Naoki Okamura (Nyarla) <nyarla@thotep.net>

This script is under the MIT-license.

